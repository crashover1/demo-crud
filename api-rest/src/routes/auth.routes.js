const { Router } = require("express");

module.exports = function ({ AuthController }) {
	const router = Router();
	router.post("/signup", AuthController.singUp);
	router.post("/signin", AuthController.singIn);

	return router;
};
