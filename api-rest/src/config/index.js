if (process.env.NODE_ENV !== "production") {
	require("dotenv").config();
}

module.exports = {
	API_PORT: process.env.API_PORT,
	MONGO_URI: process.env.MONGO_URI,
	JWT_SECRET: process.env.JWT_SECRET,
	CACHE_KEY: process.env.CACHE_KEY
};
