const mongoose = require("mongoose");
const { Schema } = mongoose;
const { compareSync, hashSync, genSaltSync } = require("bcryptjs");

const UserSchema = new Schema({
	first_name: { type: String, required: true },
	last_name: { type: String, required: true },
	second_name: { type: String },
	username: { type: String, required: true },
	password: { type: String, required: true },
});

UserSchema.methods.toJSON = function () {
	let user = this.toObject();
	delete user.password;
	return user;
};

UserSchema.methods.comparePasswords = function (password) {
	return compareSync(password, this.password);
};

UserSchema.pre("save", async function (next) {
	const user = this;
	if (!user.isModified("password")) {
		next();
	}

	const salt = genSaltSync(10);
	const hashedPassword = hashSync(user.password, salt);

	user.password = hashedPassword;
	next();
});

module.exports = mongoose.model("user", UserSchema);
