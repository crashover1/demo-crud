const mcache = require("memory-cache");
const { CACHE_KEY } = require("../config");
module.exports = function (duration, req, res, next) {
	return (req, res, next) => {
		const key = CACHE_KEY + req.originUrl || req.url;
		const cachedBody = mcache.get(key);
		if (cachedBody) {
			console.log('response with cache');
			return res.send(JSON.parse(cachedBody));
		} else {
			console.log('response without cache');
			res.sendResponse = res.send;
			res.send = (body) => {
				mcache.put(key, body, duration * 100);
				res.sendResponse(body);
			};
			next();
		}
	};
};
