const express = require("express");

let _express = null;
let _config = null;

class Server {
	constructor({ config, router }) {
		_config = config;
		_express = express().use(router);
	}

	start() {
		return new Promise((resolve) => {
			_express.listen(_config.API_PORT, () => {
				console.log(
					"DemoCrud running on PORT: " +
					_config.API_PORT
				);
			});
		});
	}
}
module.exports = Server;
