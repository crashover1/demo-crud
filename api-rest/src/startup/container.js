const { createContainer, asClass, asValue, asFunction } = require("awilix");

// config
const config = require("../config");
const app = require(".");

// services
const {
	UserService,
	AuthService
} = require("../services");
const container = createContainer();

// Controllers
const {
	UserController,
	AuthController
} = require("../controllers");

// Routes
const {
	UserRoutes,
	AuthRoutes
} = require("../routes/index.routes");
const Routes = require("../routes");

// Models
const { User } = require("../models");

// Repositories
const {
	UserRepository
} = require("../repositories");

container
	.register({
		server: asClass(app).singleton(),
		router: asFunction(Routes).singleton(),
		config: asValue(config)
	})
	.register({
		UserService: asClass(UserService).singleton(),
		AuthService: asClass(AuthService).singleton()
	})
	.register({
		UserController: asClass(
			UserController.bind(UserController)
		).singleton(),
		AuthController: asClass(AuthController.bind(AuthController)).singleton()
	})
	.register({
		UserRoutes: asFunction(UserRoutes).singleton(),
		AuthRoutes: asFunction(AuthRoutes).singleton()
	})
	.register({
		User: asValue(User)
	})
	.register({
		UserRepository: asClass(UserRepository).singleton()
	});

module.exports = container;
