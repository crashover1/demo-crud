import React from 'react';
import './index.scss';
import { Form, Formik } from "formik";
import userFormValidation from "./userForm.validation";
import userFormEditValidation from "./userFormEdit.validation";
import axios from 'axios';
import { useHistory } from 'react-router-dom';

const CUserForm = ({ userData }) => {
    const history = useHistory();
    const saveUser = (values) => {
        axios.post(process.env.REACT_APP_API_URI + '/v1/api/auth/signup', values).then(response => {
            history.push('/login')
        }).catch((error) => {
            alert('ocurio un error al guardar el usuario')
        });
    }

    const editUser = (values) => {
        axios.patch(process.env.REACT_APP_API_URI + '/v1/api/user/' + userData._id, values).then(response => {
            const data = JSON.parse(sessionStorage.getItem('user'));
            data.user = response.data;
            sessionStorage.setItem('user', JSON.stringify(data))
            window.location.replace("/");
        }).catch((error) => {
            alert('ocurio un error al guardar el usuario')
        });
    }

    return (
        <Formik
            name="personalDataForm"
            initialValues={{
                first_name: userData ? userData.first_name : '',
                second_name: userData ? userData.second_name : '',
                last_name: userData ? userData.last_name : '',
                username: userData ? userData.username : '',
                password: userData ? userData.password : '',
            }}

            validationSchema={!userData ? userFormValidation : userFormEditValidation}
            onSubmit={values => {
                !userData ? saveUser(values) : editUser(values);

            }}
        >
            {({ errors, touched, handleChange, handleBlur, values, setFieldValue, setFieldTouched, ...form }) => {
                return (
                    <Form>
                        <div className="form">
                            <h1>Datos del Usuario</h1>
                            <div className="input-group">
                                <label htmlFor="first_name">Primer nombre</label>
                                <input
                                    id="first_name"
                                    name="first_name"
                                    type="text"
                                    placeholder="Primer nombre"
                                    value={values.first_name}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                                <div className="error">{errors.first_name}</div>
                            </div>
                            <div className="input-group">
                                <label htmlFor="second_name">Segundo nombre</label>
                                <input
                                    id="second_name"
                                    name="second_name"
                                    type="text"
                                    placeholder="Segundo nombre"
                                    value={values.second_name}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                                <div className="error">{errors.second_name}</div>
                            </div>
                            <div className="input-group">
                                <label htmlFor="last_name">Primer Apellido</label>
                                <input
                                    id="last_name"
                                    name="last_name"
                                    type="text"
                                    placeholder="Primer apellido"
                                    value={values.last_name}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                                <div className="error">{errors.last_name}</div>
                            </div>
                            <div className="input-group">
                                <label htmlFor="username">Username</label>
                                <input
                                    id="username"
                                    name="username"
                                    type="text"
                                    placeholder="Nombre de usuario"
                                    value={values.username}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                                <div className="error">{errors.username}</div>
                            </div>
                            {!userData && <div className="input-group">
                                <label htmlFor="password">Password</label>
                                <input
                                    id="password"
                                    name="password"
                                    type="password"
                                    placeholder="Contraseña"
                                    value={values.password}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                                <div className="error">{errors.password}</div>
                            </div>}
                            <div className="input-group">
                                <button
                                    type="submit"
                                    disabled={!form.dirty || !form.isValid}
                                >Registrar</button>
                            </div>
                        </div>
                    </Form>
                )
            }}
        </Formik>
    );
}

export default CUserForm;