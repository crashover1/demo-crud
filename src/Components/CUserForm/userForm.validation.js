import * as Yup from 'yup';

const userFormValidation = Yup.object().shape({
    first_name: Yup.string()
        .required('El primer nombre es requerido'),
    second_name: Yup.string()
        .nullable(),
    last_name: Yup.string()
        .required('El primer Apellido es requerido'),
    username: Yup.string()
        .required('El nombre de usuaroi es requerido'),
    password: Yup.string()
        .required('la contraseña es requerida'),
});

export default userFormValidation;