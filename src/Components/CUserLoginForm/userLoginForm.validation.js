import * as Yup from 'yup';

const userLoginFormValidation = Yup.object().shape({
    username: Yup.string()
        .required('El nombre de usuaroi es requerido'),
    password: Yup.string()
        .required('la contraseña es requerida'),
});

export default userLoginFormValidation;