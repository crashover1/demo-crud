import React from 'react';
import './index.scss';
import { Form, Formik } from "formik";
import userLoginFormValidation from "./userLoginForm.validation";
import axios from 'axios';
import { useHistory } from 'react-router-dom';

const CUserLoginForm = () => {
    const history = useHistory();
    const saveUser = (values) => {
        axios.post(process.env.REACT_APP_API_URI + '/v1/api/auth/signin', values).then(response => {
            sessionStorage.setItem('user', JSON.stringify(response.data))
            window.location.replace("/");
        }).catch((error) => {
            alert('ocurio un error al guardar el usuario')
        });
    }

    return (
        <Formik
            name="personalDataForm"
            initialValues={{
                username: '',
                password: '',
            }}

            validationSchema={userLoginFormValidation}
            onSubmit={values => {
                saveUser(values)

            }}
        >
            {({ errors, touched, handleChange, handleBlur, values, setFieldValue, setFieldTouched, ...form }) => {
                return (
                    <Form>
                        <div className="form">
                            <h1>Formulario de Registro</h1>
                            <div className="input-group">
                                <label htmlFor="username">Username</label>
                                <input
                                    id="username"
                                    name="username"
                                    type="text"
                                    placeholder="Nombre de usuario"
                                    value={values.username}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                                <div className="error">{errors.username}</div>
                            </div>
                            <div className="input-group">
                                <label htmlFor="password">Password</label>
                                <input
                                    id="password"
                                    name="password"
                                    type="password"
                                    placeholder="Contraseña"
                                    value={values.password}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                                <div className="error">{errors.password}</div>
                            </div>
                            <div className="input-group">
                                <button
                                    type="submit"
                                    disabled={!form.dirty || !form.isValid}
                                >Registrar</button>
                            </div>
                        </div>
                    </Form>
                )
            }}
        </Formik >
    );
}

export default CUserLoginForm;