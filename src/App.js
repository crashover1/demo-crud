import React, { useEffect, useState } from 'react';
import axios from 'axios';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import CUserForm from './Components/CUserForm';
import CUserLoginForm from './Components/CUserLoginForm';
import { useHistory } from 'react-router-dom';
const App = () => {
    const history = useHistory();
    const [userData, setUserData] = useState(JSON.parse(sessionStorage.getItem('user')));

    const getUsers = () => {
        const config = {
            headers: {
                Authorization: userData ? userData.token : null,
            }
        }
        axios.get(process.env.REACT_APP_API_URI + '/v1/api/user', config).then(response => {
            console.log(response)
        }).catch((error) => {
            console.log('ocurrio un error al cargar los usuario');
        });
    }

    useEffect(() => {
        getUsers();
    }, []);
    return (
        <>
            <div className="container">
                <Router>
                    <nav>
                        <ul>
                            <li>
                                <Link to="/signup">Registro</Link>
                            </li>
                            <li>
                                <Link to="/login">Login</Link>
                            </li>
                        </ul>
                    </nav>
                    <Switch>
                        <Route path="/signup">
                            <CUserForm />
                        </Route>
                        <Route path="/login">
                            <CUserLoginForm />
                        </Route>
                        <Route path="/edit">
                            {userData && <CUserForm userData={userData.user} />}
                        </Route>
                        <Route path="/">
                            <div>
                                {userData && <>
                                    <h2>Nombre:</h2> {userData.user.first_name} {userData.user.second_name} {userData.user.last_name} <Link to="/edit">Editar Datos</Link>
                                </>}
                                {!userData && <h2>Por favor inicia sesión o regístrate</h2>}
                            </div>
                            {/* <CUserLoginForm /> */}
                        </Route>
                    </Switch>
                </Router>
            </div>
        </>
    );
}

export default App;